#EXTM3U url-tvg='https://iptv-org.github.io/epg/guides/pt/mi.tv.xml' 

#EXTINF: -1 tvg-logo='https://imgur.com/Xb0RMoR.png'  group-title='VARIEDADES' , PORTA DOS FUNDOS (24H)
http://service-stitcher.clusters.pluto.tv/stitch/hls/channel/5f36f2346ede750007332d11/master.m3u8?advertisingId=&appName=web&appVersion=unknown&appStoreUrl=&architecture=&buildVersion=&clientTime=0&deviceDNT=0&deviceId=9781ef22-629b-11eb-86b1-3facc9da388a&deviceMake=Chrome&deviceModel=web&deviceType=web&deviceVersion=unknown&includeExtendedEvents=false&sid=a4be72ee-4337-4176-a253-be6d837c977f&userId=&serverSideAds=true

#EXTINF: -1 tvg-logo='https://i.ibb.co/w4sTztF/tomyjerry.png'  group-title='INFANTIL' , TOM Y JERRY
https://api55.tvgratis.live/streams3/master.m3u8?id=421

#EXTINF: -1 tvg-logo='null'  group-title='24H' , VEVO R B
http://168.205.87.198:8555/live/1437/123456/218.ts

#EXTINF: -1 tvg-logo='null'  group-title='24H' , VEVO POP
http://168.205.87.198:8555/live/1437/123456/219.ts

#EXTINF: -1 tvg-logo='null'  group-title='24H' , VEVO '90S
http://168.205.87.198:8555/live/1437/123456/223.ts

#EXTINF: -1 tvg-logo='null'  group-title='24H' , VEVO '70S
http://168.205.87.198:8555/live/1437/123456/221.ts

#EXTINF: -1 tvg-logo='null'  group-title='24H' , VEVO LATINO
http://168.205.87.198:8555/live/1437/123456/220.ts

#EXTINF: -1 tvg-logo='null'  group-title='24H' , CLC FILMES CLÁSSICOS
http://168.205.87.198:8555/live/1437/123456/198.ts

#EXTINF: -1 tvg-logo='https://i.imgur.com/l8wtDXF.png'  group-title='OUTROS' , BRAZIL BRAZZERS 2
http://static.brazzers.com/scenes/5457/180sec.flv

#EXTINF: -1 tvg-logo='null'  group-title='OUTROS' , BRAZZERS
http://static.brazzers.com/scenes/3344/180sec.flv

#EXTINF: -1 tvg-logo='https://img.radios.com.br/radio/lg/radio25991_1439401628.jpg'  group-title='OUTROS' , CAMPESTRE 105.9 FM
http://str81.aovivonanet.com:8350/stream

#EXTINF: -1 tvg-logo='https://images.pluto.tv/channels/5fa15ad6367e170007cdd098/colorLogoPNG.png'  group-title='OUTROS' , FICÇÃO CIENTÍFICA | LINK 2
https://service-stitcher.clusters.pluto.tv/v1/stitch/embed/hls/channel/5fa15ad6367e170007cdd098/master.m3u8?deviceId=channel&deviceModel=web&deviceVersion=1.0&appVersion=1.0&deviceType=rokuChannel&deviceMake=rokuChannel&deviceDNT=1

#EXTINF: -1 tvg-logo='https://images.pluto.tv/channels/5fac52f142044f00078e2a51/colorLogoPNG.png'  group-title='OUTROS' , MISTÉRIOS | LINK 2
https://service-stitcher.clusters.pluto.tv/v1/stitch/embed/hls/channel/5fac52f142044f00078e2a51/master.m3u8?deviceId=channel&deviceModel=web&deviceVersion=1.0&appVersion=1.0&deviceType=rokuChannel&deviceMake=rokuChannel&deviceDNT=1

#EXTINF: -1 tvg-logo='https://logodownload.org/wp-content/uploads/2018/04/discovery-kids-logo.png'  group-title='INFANTIL' , DISCOVERY KIDS FHD¹
http://168.205.87.198:8555/live/1052/123456/143.m3u8

#EXTINF: -1 tvg-logo='http://168.205.87.198/logocanais/futura.png'  group-title='OUTROS' , FUTURA
http://168.205.87.198:8555/live/1097/123456/67.ts

#EXTINF: -1 tvg-logo='https://i.ibb.co/Tr5VJq9/telemax.png'  group-title='OUTROS' , TELEMAX
http://s5.mexside.net:1935/telemax/telemax/playlist.m3u8

#EXTINF: -1 tvg-logo='http://image.tmdb.org/t/p/w600_and_h900_bestv2/94fsjdKiz7qLW5RMDAQ0ZAvHmWY.jpg'  group-title='OUTROS' , WILLOW
http://lmtv.me:80/series/1666476743ale/289091/101088.mp4

#EXTINF: -1 tvg-logo='a'  group-title='TV ABERTA' , GLOBO
http://199.127.60.83:16020

#EXTINF: -1 tvg-logo='https://upload.wikimedia.org/wikipedia/en/2/2e/Lorenzo%27s_Oil.jpg'  group-title='FILMES' , O ÓLEO DE LORENZO (1992) - DUBLADO
https://ia803100.us.archive.org/20/items/OOleoDeLorenzo1992Dvd9Iso/OOleoDeLorenzo1992Dvd9.mp4

#EXTINF: -1 tvg-logo='null'  group-title='OUTROS' , FESTINHA NA CASA DO BRAD
https://www.bradmontana.com/trailers/festinha_na_casa_do_brad_montana.mp4

#EXTINF: -1 tvg-logo='https://distv.net/icones/space.png'  group-title='FILMES' , SPACE
http://195.181.163.144:14730

#EXTINF: -1 tvg-logo='http://46.165.222.67:2095/tourobox/sportv2.png'  group-title='ESPORTES' , SPORTV 3UHD
http://51.159.54.153:14417

#EXTINF: -1 tvg-logo='aht'  group-title='FILMES' , HBO+
http://15.235.10.31:14411

#EXTINF: -1 tvg-logo='a'  group-title='FILMES' , HBO MUNDI
http://15.235.10.31:14394

#EXTINF: -1 tvg-logo='a'  group-title='FILMES' , HBO
http://15.235.10.31:14380

#EXTINF: -1 tvg-logo='a'  group-title='FILMES' , FX
http://15.235.10.31:14340

#EXTINF: -1 tvg-logo='a'  group-title='FILMES' , CINE CANAL
http://45.58.126.146:14326

#EXTINF: -1 tvg-logo='https://i.pinimg.com/564x/4a/05/69/4a0569472ec41d50b9e0172ea0065f50.jpg'  group-title='OUTROS' , CLC FILMES CLÁSSICOS
http://168.205.87.198:8555/live/5803/123456/198.ts

#EXTINF: -1 tvg-logo='i'  group-title='FILMES' , FILMES NACIONAIS
http://service-stitcher.clusters.pluto.tv/stitch/hls/channel/5f5a545d0dbf7f0007c09408/master.m3u8?advertisingId=&appName=web&appVersion=unknown&appStoreUrl=&architecture=&buildVersion=&clientTime=0&deviceDNT=0&deviceId=a5762a61-77db-11eb-bfcd-e755c619a901&deviceMake=Chrome&deviceModel=web&deviceType=web&deviceVersion=unknown&includeExtendedEvents=false&sid=8784fb7f-5209-406d-b66a-2fe035ee1a18&userId=&serverSideAds=true

#EXTINF: -1 tvg-logo='i'  group-title='FILMES' , FILMES TERROR
http://service-stitcher.clusters.pluto.tv/stitch/hls/channel/5f12111c9e6c2c00078ef3bb/master.m3u8?advertisingId=&appName=web&appVersion=unknown&appStoreUrl=&architecture=&buildVersion=&clientTime=0&deviceDNT=0&deviceId=977ee1e0-629b-11eb-86b1-3facc9da388a&deviceMake=Chrome&deviceModel=web&deviceType=web&deviceVersion=unknown&includeExtendedEvents=false&sid=3ff7f4f3-233d-4e09-bbf7-e36a061d79f7&userId=&serverSideAds=true

#EXTINF: -1 tvg-logo='i'  group-title='FILMES' , FILMES DRAMA
http://service-stitcher.clusters.pluto.tv/stitch/hls/channel/5f1210d14ae1f80007bafb1d/master.m3u8?advertisingId=&appName=web&appVersion=unknown&appStoreUrl=&architecture=&buildVersion=&clientTime=0&deviceDNT=0&deviceId=977ebad1-629b-11eb-86b1-3facc9da388a&deviceMake=Chrome&deviceModel=web&deviceType=web&deviceVersion=unknown&includeExtendedEvents=false&sid=e2a43ad8-1b74-43eb-8a1c-af669d97a71e&userId=&serverSideAds=true

#EXTINF: -1 tvg-logo='youtube'  group-title='OUTROS' , CURSO YOUTUBE
https://www.youtube.com/playlist?list=PLHz_AreHm4dlaTyjolzCFC6IjLzO8O0XV

#EXTINF: -1 tvg-logo='https://lh3.googleusercontent.com/-6VphUlD1L4Y/X0EdNxO1YFI/AAAAAAAA42I/eJl7n6TrfWEZn2j8HSOqg1lOBrieAjlAQCK8BGAsYHg/s0/2020-08-22.png'  group-title='TV ABERTA' , BAND HD
http://168.205.87.198:8555/live/1052/123456/19.m3u8

#EXTINF: -1 tvg-logo='https://my.tizam.pw//images/cms/thumbs/975b6b07a35b5ae800ab9c0bf7f246669d338cce/28577_anal_naya_blondinka_dlya_dvojnogo_proniknoveniya_240_auto_png_0_90.png'  group-title='OUTROS' , АНАЛЬНАЯ БЛОНДИНКА ДЛ
https://video1.tizam.cc/films/AnalDPBlondeAConniePetersCollection.mp4

#EXTINF: -1 tvg-logo='http://ddsw.xyz:80/images/HM3xx55KZnCUdlPuNC1k2CQmSialG6ZFMmnpVE7A3yqcU2yQITdUn0Xo0XR3Ya3o-6AP7QSQ4YYbSQJVltXNlw.png'  group-title='ESPORTES' , SPORTV 3 SD
http://ddsw.xyz:80/0298567607/3933148466/967825.m3u8

#EXTINF: -1 tvg-logo='http://127.0.0.1/'  group-title='OUTROS' , FILMES DE NATEL (BRAZIL)
https://d8ladbldf3ey5.cloudfront.net/playlist.m3u8

#EXTINF: -1 tvg-logo='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTpqQEu3fREWhuqyEeL5w_b3iBnBDybWVBmAg&usqp=CAU'  group-title='FILMES' , FILMES ONLINE GRÁTIS
https://livefocamundo.com:8081/tvcontroles/tracks-v1a1/mono.m3u8

#EXTINF: -1 tvg-logo='null'  group-title='FILMES' , TOP MIX TV FILMES
https://video03.logicahost.com.br/filmestopmixtv/filmestopmixtv/chunklist_w333847997.m3u8

#EXTINF: -1 tvg-logo='https://i.pinimg.com/originals/e8/55/4a/e8554a57c9ef57a856901f1b9add36a0.gif'  group-title='OUTROS' , HOMEM-ARANHA 3 TVG-ID="IDDOCANALDEFILMES
http://videos.nxplay.com.br/hls/c41bf258076311eb8e1ecad7bf5d214a/video_1.m3u8

#EXTINF: -1 tvg-logo='https://images.pluto.tv/channels/5f5a545d0dbf7f0007c09408/colorLogoPNG.png'  group-title='OUTROS' , FILMES NACIONAIS | PLUTO TV BRASIL
https://service-stitcher.clusters.pluto.tv/v1/stitch/embed/hls/channel/5f5a545d0dbf7f0007c09408/master.m3u8?advertisingId={PSID}&appVersion=unknown&deviceDNT={TARGETOPT}&deviceId={PSID}&deviceLat=0&deviceLon=0&deviceMake=samsung&deviceModel=samsung&deviceType=samsung-tvplus&deviceVersion=unknown

#EXTINF: -1 tvg-logo='https://filmestorrentdownload.com.br/'  group-title='FILMES' , FILMES E SÉRIE
https://filmestorrentdownload.com.br/

#EXTINF: -1 tvg-logo='null'  group-title='OUTROS' , A CUNHADA – AS PANTERAS 480P (2007)
https://cld.pt/dl/download/e6dea354-e6c8-400f-83e3-7a6b14d4e0fc/AP_ACNHDA.avi

#EXTINF: -1 tvg-logo='https://images.pluto.tv/channels/61b790b985706b00072cb797/colorLogoPNG.png'  group-title='OUTROS' , ADRENALINA FREEZONE | PLUTO TV BRASIL
https://service-stitcher.clusters.pluto.tv/v1/stitch/embed/hls/channel/61b790b985706b00072cb797/master.m3u8?advertisingId={PSID}&appVersion=unknown&deviceDNT={TARGETOPT}&deviceId={PSID}&deviceLat=0&deviceLon=0&deviceMake=samsung&deviceModel=samsung&deviceType=samsung-tvplus&deviceVersion=unknown

#EXTINF: -1 tvg-logo='https://i.imgur.com/l8wtDXF.png'  group-title='OUTROS' , BRAZIL BRAZZERS 1
http://static.brazzers.com/scenes/7119/180sec.flv

#EXTINF: -1 tvg-logo='https://i.imgur.com/l8wtDXF.png'  group-title='OUTROS' , BRAZIL BRAZZERS 2
http://static.brazzers.com/scenes/5457/180sec.flv

#EXTINF: -1 tvg-logo='null'  group-title='OUTROS' , A NOVATA BIBI GRIFO
https://www.bradmontana.com/trailers/bibi_grifo_sem_camisinha.mp4

#EXTINF: -1 tvg-logo='null'  group-title='OUTROS' , CORNO MANSO ADORA OUVIR A ESPOSA SAFADA
https://www.bradmontana.com/trailers/LollaCorno_Trailer.mp4

#EXTINF: -1 tvg-logo='http://ddsw.xyz:80/images/HM3xx55KZnCUdlPuNC1k2NsZDpbr56w8hOM7fSmuumvHPRHj5fuqCUjwNVEYbjNQzv6QTBnvQPL7uxSGtNicHw.png'  group-title='FILMES' , AMC ••
http://199.127.60.83:16004/amc

#EXTINF: -1 tvg-logo='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQRox7t_Up8hgFk0VdzFLEm-rswGsZATMo5Cg&usqp=CAU'  group-title='FILMES' , AXN HD *
http://45.58.126.146:14299/axn

#EXTINF: -1 tvg-logo='https://br.web.img2.acsta.net/c_310_420/pictures/18/05/23/09/20/0659238.jpg'  group-title='FILMES' , PERIGO NO OCEANO
https://play.prod.gcp.vix.services/black-water_pt/black-water_pt_1-1/play_v1_hls_540p.m3u8

#EXTINF: -1 tvg-logo='https://br.web.img2.acsta.net/c_310_420/medias/nmedia/18/87/34/98/20028740.jpg'  group-title='FILMES' , A FERA
https://play.prod.gcp.vix.services/beastly_pt/beastly_pt_1-1/play_v1_hls_360p.m3u8

#EXTINF: -1 tvg-logo='https://br.web.img3.acsta.net/c_310_420/medias/nmedia/18/87/30/63/19874134.jpg'  group-title='OUTROS' , CÓDIGO DE CONDUTA
https://play.prod.gcp.vix.services/law-abiding_pt/law-abiding_pt_1-1/play_v1_hls_1080p.m3u8

#EXTINF: -1 tvg-logo='https://images.pluto.tv/channels/5d8d164d92e97a5e107638d2/colorLogoPNG-1623101979398.png'  group-title='OUTROS' , CINE ADRENALINA
http://stitcher-ipv4.pluto.tv/v1/stitch/embed/hls/channel/5d8d164d92e97a5e107638d2/master.m3u8?deviceType=samsung-tvplus&deviceMake=samsung&deviceModel=samsung&deviceVersion=unknown&appVersion=unknown&deviceLat=0&deviceLon=0&deviceDNT=%7BTARGETOPT%7D&deviceId=%7BPSID%7D&advertisingId=%7BPSID%7D&us_privacy=1YNY&samsung_app_domain=%7BAPP_DOMAIN%7D&samsung_app_name=%7BAPP_NAME%7D&profileLimit=&profileFloor=&embedPartner=samsung-tvplus

#EXTINF: -1 tvg-logo='https://images.pluto.tv/channels/5d8d164d92e97a5e107638d2/colorLogoPNG-1623101979398.png'  group-title='OUTROS' , CINE ADRENALINA
http://stitcher-ipv4.pluto.tv/v1/stitch/embed/hls/channel/5d8d164d92e97a5e107638d2/master.m3u8?deviceType=samsung-tvplus&deviceMake=samsung&deviceModel=samsung&deviceVersion=unknown&appVersion=unknown&deviceLat=0&deviceLon=0&deviceDNT=%7BTARGETOPT%7D&deviceId=%7BPSID%7D&advertisingId=%7BPSID%7D&us_privacy=1YNY&samsung_app_domain=%7BAPP_DOMAIN%7D&samsung_app_name=%7BAPP_NAME%7D&profileLimit=&profileFloor=&embedPartner=samsung-tvplus

#EXTINF: -1 tvg-logo='http://ddsw.xyz:80/images/HM3xx55KZnCUdlPuNC1k2CQmSialG6ZFMmnpVE7A3ypUWNumDsFNr42zXRAVK_cxgpnPrcz2LmoMTufnwNKZDA.png'  group-title='FILMES' , A&E HD
http://cdn.connectbr.com.br/AE/tracks-v1a1/mono.m3u8

#EXTINF: -1 tvg-logo='https://www.cxtv.com.br/img/Tvs/Logo/webp-l/084299119e2cf7e1b863489165dd5af3.webp'  group-title='INFANTIL' , TV TEEN NET  HD
https://stmv1.painelvideo.top/tvteennet/tvteennet/chunklist_w1425969400.m3u8

#EXTINF: -1 tvg-logo='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSloSMrTAde2JxLX6yT-9svx5w4OcFsFJEV1g&usqp=CAU'  group-title='OUTROS' , NICK TEEN 🚸
http://stitcher-ipv4.pluto.tv/v1/stitch/embed/hls/channel/60f5fabf0721880007cd50e3/master.m3u8?advertisingId={PSID}&appVersion=unknown&deviceDNT={TARGETOPT}&deviceId={PSID}&deviceLat=0&deviceLon=0&deviceMake=samsung&deviceModel=samsung&deviceType=samsung-tvplus&deviceVersion=unknown&embedPartner=samsung-tvplus&profileFloor=&profileLimit=&samsung_app_domain={APP_DOMAIN}&samsung_app_name={APP_NAME}&us_privacy=1YNY

#EXTINF: -1 tvg-logo='https://www.nickjrfanclub.uk/wp-content/uploads/2021/08/Nick_JR_Website_Logo_v2-1024x853.png'  group-title='24H' , NICK JR CLUB 🚸
http://stitcher-ipv4.pluto.tv/v1/stitch/embed/hls/channel/5f121460b73ac6000719fbaf/master.m3u8?advertisingId={PSID}&appVersion=unknown&deviceDNT={TARGETOPT}&deviceId={PSID}&deviceLat=0&deviceLon=0&deviceMake=samsung&deviceModel=samsung&deviceType=samsung-tvplus&deviceVersion=unknown&embedPartner=samsung-tvplus&profileFloor=&profileLimit=&samsung_app_domain={APP_DOMAIN}&samsung_app_name={APP_NAME}&us_privacy=1YNY

#EXTINF: -1 tvg-logo='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQxNrHoqspnaa2kPQzhbqai53FD4SHkavYhSw&usqp=CAU'  group-title='24H' , NICK CLASSICO. 🚸
http://stitcher-ipv4.pluto.tv/v1/stitch/embed/hls/channel/5f12151794c1800007a8ae63/master.m3u8?advertisingId={PSID}&appVersion=unknown&deviceDNT={TARGETOPT}&deviceId={PSID}&deviceLat=0&deviceLon=0&deviceMake=samsung&deviceModel=samsung&deviceType=samsung-tvplus&deviceVersion=unknown&embedPartner=samsung-tvplus&profileFloor=&profileLimit=&samsung_app_domain={APP_DOMAIN}&samsung_app_name={APP_NAME}&us_privacy=1YNY

#EXTINF: -1 tvg-logo='https://4.bp.blogspot.com/-ZflHuGLnikw/WiG4RcvcNzI/AAAAAAAAuaM/5Qr_DNoIoDU4KVrAxparxsv1q5emOf-8gCPcBGAYYCw/s400/1200px-Rede_Bandeirantes_logo_2011.svg.png'  group-title='TV ABERTA' , BAND
https://stmv1.paineltv.net/valeradiowebtv/valeradiowebtv/chunklist_w1145513024.m3u8

#EXTINF: -1 tvg-logo='https://logodownload.org/wp-content/uploads/2013/12/sbt-logo-0-1536x1536.png'  group-title='TV ABERTA' , SBT
https://stream.amsolution.net.br:8443/live/60ce4ae4b9d4c/index.m3u8

#EXTINF: -1 tvg-logo='http://ddsw.xyz:80/images/HM3xx55KZnCUdlPuNC1k2CQmSialG6ZFMmnpVE7A3ypUWNumDsFNr42zXRAVK_cxgpnPrcz2LmoMTufnwNKZDA.png'  group-title='FILMES' , A&E FULL HD
http://45.58.126.146:14293

#EXTINF: -1 tvg-logo='https://br.web.img3.acsta.net/pictures/210/127/21012766_20130617094249647.jpg'  group-title='OUTROS' , O REI MACACO: A LENDA COMEÇA
THE MONKEY
https://s22.pandafiles.com:183/d/543t6npc26kwoyapcoe3g2wgggzp5ops2tbvptv6wb54izlnatfokez2srkvdeul4p76dl2h/O.Rei.Macaco.720p.MP4.DUB.BaixarSeriesMP4.xyz.mp4

#EXTINF: -1 tvg-logo='https://imgs3.pongalo.com/cdn-cgi/image/q=90,fit=crop,w=200,format=auto/ouija-experiment-2_pt/ouija-experiment-2_pt_200x300.jpg'  group-title='OUTROS' , OUIJA - A RESSURREIÇÃO
https://play.prod.gcp.vix.services/ouija-experiment-2_pt/ouija-experiment-2_pt_1-1/play_hls_720p.m3u8

#EXTINF: -1 tvg-logo=''  group-title='ESPORTES' , BR: SPORTV HD
http://ttv.bz:80/xgennnnldfsdnvsdvidufvndasnn/TzGCKJYsl9/980

#EXTINF: -1 tvg-logo='https://i.imgur.com/dal1lDK.png'  group-title='OUTROS' , BAND MAIS
https://enc1.proxy.tokplay.com.br/BAND-MAIS-HD/index.m3u8

#EXTINF: -1 tvg-logo='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSQF674ssOiLRDkwhPGQaWYEnXtHedNWZzZPQ&usqp=CAU.png'  group-title='FILMES MP4' , NO LUGAR ERRADO
https://firebasestorage.googleapis.com/v0/b/filmes-e-series-4e0a5.appspot.com/o/No%20Lugar%20Errado%202022%2FWrong.Place.720p.MP4.DUB.BaixarSeriesMP4.xyz.mp4?alt=media&token=629fd55f-375e-4c3d-b433-8573e6d4ec85

#EXTINF: -1 tvg-logo='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQm3X8qA1xtRrX9euaURhxqN2lxwWHNUCNbiVGTkGwbdwIIvryeTcD_EQg&s=10.png'  group-title='FILMES MP4' , DEPOIS DO UNIVERSO
https://firebasestorage.googleapis.com/v0/b/filmes-e-series-4e0a5.appspot.com/o/Depois%20do%20Universo%202022%2FBeyond.the.Universe.2022.PORTUGUESE.mp4?alt=media&token=171f0aaf-e7b0-48fd-b907-5f3c05c0d847

#EXTINF: -1 tvg-logo='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSgZIzZXKOVVDIo9YDJNdiGO6CEdH15z9uFdqKaKsjOpSFOLFCWJSZlb4k&s=10.png'  group-title='FILMES MP4' , A CINCO PASSOS DE VOCÊ
https://firebasestorage.googleapis.com/v0/b/filmes-e-series-4e0a5.appspot.com/o/A%20cinco%20passos%20de%20voc%C3%AA%2FA.Cinco.Passos.de.Voc%C3%AA.2019.MP4.DUB.BaixarFilmesMP4.net.mp4?alt=media&token=9d7f9c26-9714-46f4-93f9-ba6bee8254f4

#EXTINF: -1 tvg-logo='https://br.web.img3.acsta.net/pictures/21/04/08/16/25/5256018.jpg'  group-title='FILMES MP4' , A LENDA DE THOR
https://firebasestorage.googleapis.com/v0/b/filmes-e-series-4e0a5.appspot.com/o/A%20LENDA%20DE%20THOR%2FA.Lenda.de.Thor.2020.MP4.DUB.BaixarSeriesMP4.org.mp4?alt=media&token=1a3980ec-d741-41c9-b0d0-bb78183eef85

#EXTINF: -1 tvg-logo='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSJ8BKiEDXW4ubePTqvdQaXLXBKhiYUAu4a_g&usqp=CAU.png'  group-title='FILMES MP4' , THE PRINCESS
https://firebasestorage.googleapis.com/v0/b/filmes-e-series-4e0a5.appspot.com/o/THE%20PRINCESS%2FA.Princesa.2022.720p.MP4.DUB.BaixarSeriesMP4.Club.mp4?alt=media&token=81c805c3-5e76-4d7b-aaf3-d3f1ec199229

#EXTINF: -1 tvg-logo='http://i.imgur.com/7SlZF9B.png'  group-title='OUTROS' , SBT PE HD
http://evpp.mm.uol.com.br:1935/ne10/ne10.smil/chunklist_w416898237_b216000_sleng.m3u8

#EXTINF: -1 tvg-logo='https://listaiptv.gratis/logos/imagens/band.png'  group-title='TV ABERTA' , BAND CE HD
http://lxls.mx:80/giselebello/80754546/119051

#EXTINF: -1 tvg-logo='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQVSgHCpm3v6oYIQRMjXhRSfo3gh6wHB8xOTw&usqp.jpg'  group-title='TV ABERTA' , GÊNESIS FHD
http://lxls.mx:80/giselebello/80754546/333109

#EXTINF: -1 tvg-logo='http://xcui.vitvabc.xyz:8880/images/39ba71210c63301ed9b037b9d22e77d7.png'  group-title='ESPORTES' , SPORTV
http://209.222.97.92:16525

#EXTINF: -1 tvg-logo=''  group-title='ESPORTES' , BRA DEP | ESPN
http://soberon.us:25461/m0nk/n3w/4098

#EXTINF: -1 tvg-logo=''  group-title='ESPORTES' , BRA DEP | ESPN 2
http://soberon.us:25461/m0nk/n3w/4099

#EXTINF: -1 tvg-logo=''  group-title='ESPORTES' , BRA DEP | FOX SPORTS
http://soberon.us:25461/m0nk/n3w/4100

#EXTINF: -1 tvg-logo=''  group-title='FILMES E SÉRIES' , BRA HD | A&E
http://soberon.us:25461/m0nk/n3w/4102

#EXTINF: -1 tvg-logo=''  group-title='ESPORTES' , BRA HD | BANDS SPORTS
http://soberon.us:25461/m0nk/n3w/4103

#EXTINF: -1 tvg-logo=''  group-title='ESPORTES' , BRA HD | COMBATE
http://soberon.us:25461/m0nk/n3w/4105

#EXTINF: -1 tvg-logo=''  group-title='DOCUMENTÁRIOS' , BRA HD | DISCOVERY CHANNEL
http://soberon.us:25461/m0nk/n3w/4107

#EXTINF: -1 tvg-logo=''  group-title='DOCUMENTÁRIOS' , BRA HD | DISCOVERY H&H
http://soberon.us:25461/m0nk/n3w/4108

#EXTINF: -1 tvg-logo=''  group-title='FILMES E SÉRIES' , BRA HD | FOX
http://soberon.us:25461/m0nk/n3w/4109

#EXTINF: -1 tvg-logo=''  group-title='TV ABERTA' , BRA HD | GLOBO MINAS
http://soberon.us:25461/m0nk/n3w/4110

#EXTINF: -1 tvg-logo=''  group-title='NOTICIAS' , BRA HD | GLOBO NEWS
http://soberon.us:25461/m0nk/n3w/4111

#EXTINF: -1 tvg-logo=''  group-title='FILMES E SÉRIES' , BRA HD | HBO
http://soberon.us:25461/m0nk/n3w/4112

#EXTINF: -1 tvg-logo=''  group-title='FILMES E SÉRIES' , BRA HD | HBO 2
http://soberon.us:25461/m0nk/n3w/4113

#EXTINF: -1 tvg-logo=''  group-title='FILMES E SÉRIES' , BRA HD | HBO FAMILY
http://soberon.us:25461/m0nk/n3w/4114

#EXTINF: -1 tvg-logo=''  group-title='FILMES E SÉRIES' , BRA HD | HBO SIGNATURE
http://soberon.us:25461/m0nk/n3w/4116

#EXTINF: -1 tvg-logo=''  group-title='DOCUMENTÁRIOS' , BRA HD | HYSTORY
http://soberon.us:25461/m0nk/n3w/4118

#EXTINF: -1 tvg-logo=''  group-title='VARIEDADES' , BRA HD | MULTISHOW
http://soberon.us:25461/m0nk/n3w/4119

#EXTINF: -1 tvg-logo=''  group-title='DOCUMENTÁRIOS' , BRA HD | NATIONAL GEOGRAPHIC
http://soberon.us:25461/m0nk/n3w/4121

#EXTINF: -1 tvg-logo=''  group-title='FILMES E SÉRIES' , BRA HD | PARAMOUNT
http://soberon.us:25461/m0nk/n3w/4122

#EXTINF: -1 tvg-logo=''  group-title='ESPORTES' , BRA HD | PREMIERE
http://soberon.us:25461/m0nk/n3w/4123

#EXTINF: -1 tvg-logo=''  group-title='TV ABERTA' , BRA HD | RECORD
http://soberon.us:25461/m0nk/n3w/4124

#EXTINF: -1 tvg-logo=''  group-title='FILMES E SÉRIES' , BRA HD | SPACE
http://soberon.us:25461/m0nk/n3w/4127

#EXTINF: -1 tvg-logo=''  group-title='TV ABERTA' , BRA HD | STB JORNAL
http://soberon.us:25461/m0nk/n3w/4131

#EXTINF: -1 tvg-logo=''  group-title='FILMES E SÉRIES' , BRA HD | SYFY
http://soberon.us:25461/m0nk/n3w/4132

#EXTINF: -1 tvg-logo=''  group-title='FILMES E SÉRIES' , BRA HD | TELECINE PREMIUN
http://soberon.us:25461/m0nk/n3w/4135

#EXTINF: -1 tvg-logo=''  group-title='FILMES E SÉRIES' , BRA HD | TNT
http://soberon.us:25461/m0nk/n3w/4136

#EXTINF: -1 tvg-logo=''  group-title='FILMES E SÉRIES' , BRA HD | TNT SERIES
http://soberon.us:25461/m0nk/n3w/4137

#EXTINF: -1 tvg-logo=''  group-title='TV ABERTA' , BRA HD | TV GLOBO INTERNACIONA...
http://soberon.us:25461/m0nk/n3w/4138

#EXTINF: -1 tvg-logo=''  group-title='VARIEDADES' , BRA HD | VIVA
http://soberon.us:25461/m0nk/n3w/4139

#EXTINF: -1 tvg-logo=''  group-title='INFANTIL' , BRA KIDS | BABY TV
http://soberon.us:25461/m0nk/n3w/4140

#EXTINF: -1 tvg-logo=''  group-title='INFANTIL' , BRA KIDS | BOOMERAN
http://soberon.us:25461/m0nk/n3w/4141

#EXTINF: -1 tvg-logo=''  group-title='INFANTIL' , BRA KIDS | DISCOVERY KIDS
http://soberon.us:25461/m0nk/n3w/4142

#EXTINF: -1 tvg-logo=''  group-title='INFANTIL' , BRA KIDS | DISNEY CHANNEL
http://soberon.us:25461/m0nk/n3w/4143

#EXTINF: -1 tvg-logo=''  group-title='INFANTIL' , BRA KIDS | NATGEO KIDS
http://soberon.us:25461/m0nk/n3w/4144

#EXTINF: -1 tvg-logo=''  group-title='INFANTIL' , BRA KIDS | NICK JR
http://soberon.us:25461/m0nk/n3w/4145

#EXTINF: -1 tvg-logo=''  group-title='INFANTIL' , BRA KIDS | TOONCAST
http://soberon.us:25461/m0nk/n3w/4146

#EXTINF: -1 tvg-logo=''  group-title='ESPORTES' , BRA | DAZN 2 HD
http://soberon.us:25461/m0nk/n3w/4148

#EXTINF: -1 tvg-logo=''  group-title='NOTICIAS' , BRA | GLOBO NEWS SD
http://soberon.us:25461/m0nk/n3w/4150

#EXTINF: -1 tvg-logo=''  group-title='ESPORTES' , BRA | SPORTV 1 HD
http://soberon.us:25461/m0nk/n3w/4152

#EXTINF: -1 tvg-logo=''  group-title='ESPORTES' , BRA | SPORTV 2 HD
http://soberon.us:25461/m0nk/n3w/4153

#EXTINF: -1 tvg-logo=''  group-title='ESPORTES' , BRA | SPORTV 3 HD
http://soberon.us:25461/m0nk/n3w/4154

#EXTINF: -1 tvg-logo=''  group-title='ESPORTES' , DEP | PREMIERE 1 HD BRA
http://soberon.us:25461/m0nk/n3w/3519

#EXTINF: -1 tvg-logo=''  group-title='ESPORTES' , DEP  | BAND SPORTS BRA
http://soberon.us:25461/m0nk/n3w/3515

#EXTINF: -1 tvg-logo=''  group-title='ESPORTES' , DEP | PREMIERE 3  BRA
http://soberon.us:25461/m0nk/n3w/3520

#EXTINF: -1 tvg-logo=''  group-title='ESPORTES' , DEP | PREMIERE 5 HD BRA
http://soberon.us:25461/m0nk/n3w/3522

#EXTINF: -1 tvg-logo=''  group-title='ESPORTES' , DEP | PREMIERE 7 BRA
http://soberon.us:25461/m0nk/n3w/3524

#EXTINF: -1 tvg-logo=''  group-title='ADULTOS' , XXX | ADULTIPTV.NET BRUNETTE
http://soberon.us:25461/m0nk/n3w/3209

#EXTINF: -1 tvg-logo=''  group-title='ADULTOS' , XXX | BRAZZERS
http://soberon.us:25461/m0nk/n3w/390

#EXTINF: -1 tvg-logo='https://lh3.googleusercontent.com/-_mVYIdXLYWQ/YGuFFxxUHFI/AAAAAAABBOo/1u_EVaReRtY31sRUcng-usb7bVQ9krgKwCK8BGAsYHg/s512/2021-04-05.jpg'  group-title='ADULTOS' , MUSIC | SEXY KPOP TV | BR
http://soberon.us:25461/m0nk/n3w/3657

#EXTINF: -1 tvg-logo='https://i.imgur.com/H8G45iZ.png'  group-title='TV ABERTA' , BAND SP HD
http://sukitam.bccdn.xyz:80/740940441685/872339776043/324

#EXTINF: -1 tvg-logo='http://ccdnblck.xyz:80/images/58813ad226c2e4f1598265378eefb919.jpg'  group-title='ADULTOS' , XXX 24 HORAS DE PRAZER
http://sukitam.bccdn.xyz:80/movie/740940441685/872339776043/10614.mp4

#EXTINF: -1 tvg-logo='http://ccdnblck.xyz:80/images/58813ad226c2e4f1598265378eefb919.jpg'  group-title='ADULTOS' , XXX A ESTOQUISTA
http://sukitam.bccdn.xyz:80/movie/740940441685/872339776043/10620.mp4

#EXTINF: -1 tvg-logo='http://ccdnblck.xyz:80/images/58813ad226c2e4f1598265378eefb919.jpg'  group-title='ADULTOS' , XXX A NOVA CUNHADA
http://sukitam.bccdn.xyz:80/movie/740940441685/872339776043/10629.mp4

#EXTINF: -1 tvg-logo='http://ccdnblck.xyz:80/images/58813ad226c2e4f1598265378eefb919.jpg'  group-title='ADULTOS' , XXX A RENEGADA
http://sukitam.bccdn.xyz:80/movie/740940441685/872339776043/10631.mp4

#EXTINF: -1 tvg-logo='http://image.tmdb.org/t/p/w600_and_h900_bestv2/oyGDaoPPEonZSlzelj8SckcHj9H.jpg'  group-title='FILMES E SÉRIES' , A ANATOMIA DE GREY S19 E06
http://sukitam.bccdn.xyz:80/series/740940441685/872339776043/84315.mp4

#EXTINF: -1 tvg-logo='https://i.postimg.cc/ncKFw3mz/BRA-GLOBO-BRASILIA.png'  group-title='OUTROS' , GLOBO
http://15.204.174.61:8000/play/a00b/index.m3u8

#EXTINF: -1 tvg-logo='https://i.postimg.cc/8zrHSr1j/BRA-SBT.png'  group-title='OUTROS' , SBT
http://200.4.96.47:8000/play/a014/index.m3u8

#EXTINF: -1 tvg-logo='https://image.tmdb.org/t/p/w600_and_h900_bestv2/sKfmovp5r37SBrLN9ga8QxsETQM.jpg'  group-title='OUTROS' , A ANATOMIA DE GREY S18 E06
http://p23.miami:80/series/Kinge607/King.e607/273047.mp4

#EXTINF: -1 tvg-logo=''  group-title='OUTROS' , 24HS POCOYO
http://milka99.xyz:80/gWTS5z5Pgq/Z0ogH89m5o/62861

#EXTINF: -1 tvg-logo=''  group-title='OUTROS' , 24HS TICO E TECO
http://milka99.xyz:80/gWTS5z5Pgq/Z0ogH89m5o/70959

#EXTINF: -1 tvg-logo=''  group-title='OUTROS' , GLOBO
http://milka99.xyz:80/gWTS5z5Pgq/Z0ogH89m5o/65209

#EXTINF: -1 tvg-logo=''  group-title='OUTROS' , SPORTV HD¹
http://milka99.xyz:80/gWTS5z5Pgq/Z0ogH89m5o/39445

#EXTINF: -1 tvg-logo=''  group-title='OUTROS' , BBB MOSAICO
http://milka99.xyz:80/gWTS5z5Pgq/Z0ogH89m5o/87417

#EXTINF: -1 tvg-logo=''  group-title='OUTROS' , GLOBO RIO SUL HD
http://milka99.xyz:80/gWTS5z5Pgq/Z0ogH89m5o/73164

#EXTINF: -1 tvg-logo='null'  group-title='OUTROS' , CAMPEÃ ANAL
https://www.bradmontana.com/trailers/elisa_sanches_dpa.mp4

#EXTINF: -1 tvg-logo=''  group-title='OUTROS' , XXX: MEGA 3)·****·
http://milka99.xyz:80/gWTS5z5Pgq/Z0ogH89m5o/68937

#EXTINF: -1 tvg-logo=''  group-title='OUTROS' , STAR+ 1³
http://milka99.xyz:80/gWTS5z5Pgq/Z0ogH89m5o/73401

#EXTINF: -1 tvg-logo=''  group-title='OUTROS' , STAR+ 2³
http://milka99.xyz:80/gWTS5z5Pgq/Z0ogH89m5o/73402

#EXTINF: -1 tvg-logo=''  group-title='OUTROS' , STAR+ 3³
http://milka99.xyz:80/gWTS5z5Pgq/Z0ogH89m5o/73403

#EXTINF: -1 tvg-logo=''  group-title='OUTROS' , ESPN HD¹
http://milka99.xyz:80/gWTS5z5Pgq/Z0ogH89m5o/6825

#EXTINF: -1 tvg-logo=''  group-title='OUTROS' , ESPN FHD¹
http://milka99.xyz:80/gWTS5z5Pgq/Z0ogH89m5o/15439

#EXTINF: -1 tvg-logo='https://i.imgur.com/Xlf6PBQ.png'  group-title='OUTROS' , MEGAPIX HD
http://51.83.238.223:16823/megapix

#EXTINF: -1 tvg-logo='https://i.imgur.com/QhSS71Q.png'  group-title='OUTROS' , FM O DIA
http://streaming.livespanel.com:20000/live

#EXTINF: -1 tvg-logo='https://i.ibb.co/Pm0XY0P/24hdesenhos.png'  group-title='OUTROS' , 24H A CASA DO MICKEY MOUSE
http://suatv.fun:80/438259883931/338764001182/486528

#EXTINF: -1 tvg-logo='https://i.postimg.cc/pV6zLXc8/BRA-TELE-PIPOCA-HD.png'  group-title='OUTROS' , TELECINE PIPOCA
http://186.237.99.202:8000/play/TCPipoca/index.m3u8

#EXTINF: -1 tvg-logo='https://i.postimg.cc/KjRTDdjR/BRA-TELE-ACTION-HD.png'  group-title='OUTROS' , TELECINE ACTION
http://186.237.99.202:8000/play/TCAction/index.m3u8

#EXTINF: -1 tvg-logo='https://i.postimg.cc/JzwXjYLz/BRA-TELE-PREM-HD.png'  group-title='OUTROS' , TELECINE PREMIUM
http://186.237.99.202:8000/play/TCPremium/index.m3u8

#EXTINF: -1 tvg-logo='https://i.postimg.cc/Bvp19q1j/BRA-TELECINE-TOUCH.png'  group-title='OUTROS' , TELECINE TOUCH
http://186.237.99.202:8000/play/TCTouch/index.m3u8

#EXTINF: -1 tvg-logo='http://floriu.com/capasfilmes/tctouch.png'  group-title='OUTROS' , TELECINE TOUCH
http://sv1.casaplay5g.win:8080/andre123/andre123/4026

#EXTINF: -1 tvg-logo='https://img.tvcl.xyz/brasil/telecine.png'  group-title='OUTROS' , BR: TELECINE FUN
http://dnsplay.xyz:80/fbwnbjeb/Eb5uc33r5A/304191

#EXTINF: -1 tvg-logo='https://logosave.com/images/large/17/TV-Brasil-logo.png'  group-title='OUTROS' , TV SPORTV
http://fadder.eletrinho.shop:2095/live/sportv/chunks.m3u8?token=1679952604_e943af554dd4354bf4baa7f4f288732044bc3405

#EXTINF: -1 tvg-logo='https://i.imgur.com/f2pJ8uW.png'  group-title='OUTROS' , A&E HD
http://meuplay.shop:80/neomotosbispo/46756179/1

#EXTINF: -1 tvg-logo=''  group-title='OUTROS' , ADULTIPTV.NET LATINA (720P)
http://cdn.adultiptv.net/latina.m3u8

#EXTINF: -1 tvg-logo='https://files.adultiptv.net/adultiptvnet.jpg'  group-title='RWFAEL' , CANAL
https://cdn.adultiptv.net/hardcore.m3u8

